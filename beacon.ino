/*********************************************************************
Beacon.ino
Crea un Beacon BLE y hace broadcast de la lectura del sensor
Joan Ciprià Moreno Teodoro
10/10/2019
GPL v3
*********************************************************************/

// Librerías propias
#include "EmisoraBLE.h"
#include "SensorAzufre.h"

// Librerías para SparkFun Pro nRF52840 Mini
#include <Adafruit_LittleFS.h>
#include <InternalFileSystem.h>

// Creamos un objeto emisora BLE
EmisoraBLE emisora;

// Creamos un objeto sensor azufre
SensorAzufre sensor1(9600);

void setup()
{
   Serial.begin(9600);
   while ( !Serial ) delay(10); // Pequeño delay para el nrf52840 con usb native  
  
  // Iniciamos el sensor
   sensor1.inicializar();

  // Inicializamos la emisora
  emisora.inicializar();
}

void loop()
{
    // Medimos y publicamos cada 2 segundos
    medirYPublicar();
    delay(2000);  
}

void medirYPublicar(){
    // Leemos el sensor para obtener el azufre
    sensor1.dimeloTodo();
    int azufre = sensor1.dimeMedidaAzufre();
    int temperatura = sensor1.dimeTemperatura();

    // Anunciamos el nuevo valor
    emisora.anunciarSO2(azufre, temperatura);
}
