/*********************************************************************
EmisoraBLE.h
Crea un beacon BLE con el cual hace broadcast del valor indicado,
utilizando los bytes MAJOR y MINOR
Joan Ciprià Moreno Teodoro
10/10/2019
GPL v3
*********************************************************************/

// Librería BLE para SparkFun Pro nRF52840 Mini
#include <bluefruit.h>

// BLE Service
BLEBeacon elBeacon;

uint8_t beaconUUID[16] = {
    'E', 'P', 'S', 'G', '-', 'G', 'T', 'I',
    '-', 'E', 'Q', 'U', 'I', '-', '0', '1'};

class EmisoraBLE
{
public:
  int MAJOR = 0; // Aquí ira el valor del sensor en ppb
  int MINOR = 0; // Aquí irá la temperatura

  EmisoraBLE(){

  };

  void inicializar()
  {
    Bluefruit.begin();
    Bluefruit.setName("Beacon-Equipo1");
    Bluefruit.ScanResponse.addName();
    empezarAanunciarse();
  }

  void empezarAanunciarse(void)
  {
    Bluefruit.Advertising.stop();  // Paramos de anunciarnos para configurar
    Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
    Bluefruit.Advertising.addTxPower();
    Bluefruit.Advertising.addName();

    BLEBeacon elBeacon(beaconUUID, MAJOR, MINOR, 73);
    elBeacon.setManufacturer(0x004c); // aple id
    Bluefruit.Advertising.setBeacon(elBeacon);
    Bluefruit.Advertising.restartOnDisconnect(true);
    Bluefruit.Advertising.setInterval(32, 244); // in unit of 0.625 ms
    Bluefruit.Advertising.setFastTimeout(30);   // number of seconds in fast mode
    Bluefruit.Advertising.start(0);             // Volvemos a anunciarnos. 0 = Don't stop advertising after n seconds
  }

  void anunciarSO2(int major, int minor)
  {
    // Actualizamos valor
    MAJOR = major;
    MINOR = minor;
    Serial.print("Major: ");
    Serial.println(MAJOR);
    Serial.print("Minor: ");
    Serial.println(MINOR);
    // Actualizamos trama
    BLEBeacon elBeacon(beaconUUID, MAJOR, MINOR, 73); 
    Bluefruit.Advertising.setBeacon(elBeacon);
  }
};
