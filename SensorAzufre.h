/*********************************************************************
SensorAzufre.h
Lee el sensor de azufre
Diana Hernández Soler
23/10/2019
GPL v3
*********************************************************************/
#ifndef SensorAzufre_h
#define SensorAzufre_h
 
#include "Arduino.h"

 class SensorAzufre {
  
  // ----------------------------------------------------------
  // Variables privadas
  // ----------------------------------------------------------
  private: 
    long baudios;
    int sensorData [11];
    int idSensor;
    int ppb;
    int temperatura;
    int humedad;
    int rawSensor;
    int temperaturaDigital;
    int humedadDigital; 
    int dias;
    int horas;
    int minutos;
    int segundos;
    String tiempoActivo;

  public:
    SensorAzufre(long baudios_)
    {
      baudios = baudios_;
    }
    
    //----------------------------------------------------------------------
    // Inicialización del Serial para depuración y del serial 1 para la comunicación con el sensor
    // -> f() -> 
    //----------------------------------------------------------------------
    void inicializar()
    { 
      // Serial.begin(baudios);
      // while ( !Serial ) delay(10); 
      // Serial 1
      Serial1.begin(baudios);
    }
    
    //----------------------------------------------------------------------
    // Método que lee los datos del sensor de azufre
    // -> f() ->  
    // Muestra output del sensor:
    // [0] => SN [XXXXXXXXXXXX], 
    // [1] =>PPB [0 : 999999], 
    // [2] => TEMP [-99 : 99],
    // [3] => RH [0 : 99], 
    // [4] => RawSensor[ADCCount], 
    // [5] => TempDigital, RHDigital, 
    // [6] => Day [0 : 99], 
    // [7] => Hour [0 : 23], 
    // [9] => Minute [0 : 59], 
    // [10] => Second [0 : 59]
    //----------------------------------------------------------------------
    void leerSensor()
    {
      // Inicia una lectura del sensor
      Serial1.print('\r'); 
      
      // Serial.println("Lectura del sensor iniciada...esperando...");
      Serial1InParser();
      idSensor = sensorData[0];
      ppb = sensorData[1];
      temperatura = sensorData[2];
      humedad = sensorData[3];   
      rawSensor = sensorData[4];
      temperaturaDigital = sensorData[5];   
      humedadDigital = sensorData[6];
      dias = sensorData[7];
      horas = sensorData[8];
      minutos = sensorData[9];
      segundos = sensorData[10];
      tiempoActivo = (String) dias + "d, " + horas + "h, " + minutos + "m, " + segundos + "s";
      // Serial.println(sensorData[j]);
      
    }


    //----------------------------------------------------------------------
    // Recibe una cadena del sensor digital en forma de secuencia ASCII.
    // -> f() -> 
    //----------------------------------------------------------------------
    void Serial1InParser(void)
    {
        int i = 0;
        for (int i =0; i<11; i++) {
           while(!Serial1.available()) {
              //Serial.println("No está disponible");
            }
           sensorData[i] = Serial1.parseInt();
        }
    }

    //----------------------------------------------------------------------
    // Muestra por el serial todos los datos que ofrece el sensor
    // -> f() -> 
    //----------------------------------------------------------------------
    void dimeloTodo(){
      
      // coge el valor sensorData[] y lo separa
      leerSensor();
      Serial.println("====================================");
      Serial.println("============ dimeloTodo() ==========");
      Serial.println("====================================");
      Serial.print("idSensor: ");
      Serial.println(idSensor);
      Serial.print("Ppb: ");
      Serial.println(ppb);
      Serial.print("Temperatura: ");
      Serial.println(temperatura);
      Serial.print("Humedad: ");
      Serial.println(humedad);
      Serial.print("Raw:");
      Serial.println(rawSensor);
      Serial.print("Temperatura digital:");
      Serial.println(temperaturaDigital);
      Serial.print("Humedad digital:");
      Serial.println(humedadDigital);
      Serial.print("Sensor activo desde hace ");
      Serial.println(tiempoActivo);
    }
    
//------------------------------------------------------------------------
// GETTERS DE LOS ATRIBUTOS DE LA CLASE
//------------------------------------------------------------------------
    int dimeSerial() {
        return idSensor;
    }

    int dimeMedidaAzufre() {
        return ppb;
    }

    int dimeTemperatura() {
        return temperatura;
    }

    int dimeHumedad() {
        return humedad;
    }

    String dimeTiempoActivo() {
        return tiempoActivo;
    }

    int dimeRaw() {
        return rawSensor;
    }

    int dimeTemperaturaDigital() {
        return temperaturaDigital;
    }

    int dimeHumedadDigital() {
        return humedadDigital;
    }
   
 };
#endif
